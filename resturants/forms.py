from django import forms
from .models import ResturantLocation


class ResturantCreateForm(forms.Form):
    name = forms.CharField(max_length=120)
    location = forms.CharField(max_length=120, required=False)
    category = forms.CharField(max_length=120, required=False)


class ResturantLocationModelForm(forms.ModelForm):
    class Meta:
        model = ResturantLocation
        fields = [
            "name",
            "location",
            "category"
        ]