from django.shortcuts import render, get_object_or_404
from django.views import View
from django.http import HttpResponseRedirect, Http404
from django.views.generic import TemplateView, ListView, DetailView, CreateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy

from .models import ResturantLocation
from .forms import ResturantCreateForm, ResturantLocationModelForm


# Create your views here.
class ResturantListView(LoginRequiredMixin, ListView):
    login_url = reverse_lazy("userauth:login")
    template_name = "resturants/resturant_list.html"

    def get_queryset(self):
        user = self.request.user
        if user.is_authenticated:
            queryset = ResturantLocation.objects.filter(owner__username__exact=user)
        else:
            raise Http404
        return queryset


class ResturantDetailView(DetailView):
    def get_object(self, queryset=None):
        slug = self.kwargs.get("slug")
        obj = get_object_or_404(ResturantLocation, slug=slug)
        return obj


class ResturantCreateView(LoginRequiredMixin, CreateView):
    login_url = reverse_lazy("userauth:login")
    form_class = ResturantLocationModelForm
    template_name = "resturants/form.html"
    success_url = reverse_lazy("resturants:list")

    def form_valid(self, form):
        instance = form.save(commit=False)
        instance.owner =self.request.user
        return super().form_valid(form)

