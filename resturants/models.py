from django.db import models
from django.db.models.signals import pre_save, post_save
from .utils import unique_slug_generator
from .validators import validate_resturant_category, validate_resturant_name
from userauth.models import CustomUser


# Create your models here.
class ResturantLocation(models.Model):
    name = models.CharField(max_length=120, validators=[validate_resturant_name])
    location = models.CharField(max_length=120, null=True, blank=True)

    category = models.CharField(max_length=120, null=True, blank=True, validators=[validate_resturant_category])
    owner = models.ForeignKey(CustomUser, blank=True, null=True, on_delete=models.CASCADE)
    timestamp = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    slug = models.SlugField(null=True, blank=True)


    def __str__(self):
        return self.name


def pre_save_receiver(sender, instance, *args, **kwargs):
    print("saving {}".format(instance))
    if not instance.slug:
        instance.slug = unique_slug_generator(instance)
    instance.category = instance.category.capitalize()


def post_save_receiver(sender, instance, created, *args, **kwargs):
    print("saved {}".format(created))


pre_save.connect(pre_save_receiver, sender=ResturantLocation)

post_save.connect(post_save_receiver, sender=ResturantLocation)
