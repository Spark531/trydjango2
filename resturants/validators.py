from django.core.exceptions import ValidationError


CATEGORIES = ["veg", "non-veg"]
def validate_resturant_name(resturant_name):
    if resturant_name == "create":
        raise ValidationError(f"{resturant_name} cannot be used as a Resturant name.")


def validate_resturant_category(category):
    if category not in CATEGORIES:
        raise ValidationError(f"{category} is not a valid category. Use either of {CATEGORIES}")