# users/views.py
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.edit import CreateView
from django.contrib.auth.views import LoginView
from django.views.generic import DetailView, UpdateView
from django.shortcuts import render, get_object_or_404
from django.urls import reverse_lazy

from .forms import CustomUserCreationForm, CustomUserUpdateForm
from .models import CustomUser

class SignUpView(CreateView):
    form_class = CustomUserCreationForm
    template_name = 'userauth/signup.html'
    def get_success_url(self):
        return reverse_lazy("userauth:login")


class UserLoginView(LoginView):
    template_name = 'userauth/login.html'
    redirect_authenticated_user=True
    def get_success_url(self):
        return reverse_lazy("userauth:profile")


class UserProfileView(LoginRequiredMixin, DetailView):
    login_url = reverse_lazy("userauth:login")
    template_name = "userauth/profile.html"

    def get_object(self, queryset=None):
        username = self.request.user.get_username()
        user_detail = get_object_or_404(CustomUser, username=username)
        return user_detail

class UserProfileUpdateView(LoginRequiredMixin, UpdateView):
    login_url = reverse_lazy("userauth:login")
    form_class = CustomUserUpdateForm
    template_name = "userauth/update.html"

    def get_object(self, queryset=None):
        username = self.request.user.get_username()
        user_detail = get_object_or_404(CustomUser, username=username)
        return user_detail

    def get_success_url(self):
        return reverse_lazy("userauth:profile")